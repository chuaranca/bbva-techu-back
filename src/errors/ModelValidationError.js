'use strict';

import { STATUS_WARNING } from "../shared/constans";

export default class ModelValidationError extends Error {
  constructor(error) {
    super(error);
    this.status = 200;
    this.name = "ModelValidationError";
  }

  toJson() {
    return {
      status: STATUS_WARNING,
      message: this.message,
    };
  }
}
