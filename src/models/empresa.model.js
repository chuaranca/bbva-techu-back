'use strict';

import mongoose from "mongoose";
import telefonoSchema from "./telefono.schema";
import bcrypt from "bcryptjs";

const usuarioSchema = mongoose.Schema({
  _id: false,
  codigoUsuario: { type: String, required: true, maxlength: 8, default: "ADMIN001" },
  password: { type: String, required: true },
  nombres: { type: String, required: true },
  apellidos: { type: String, required: true },
  email: { type: String, required: true },
  telefono: { type: telefonoSchema, required: true },
  estado: { type: String, enum: ["A", "I"], default: "A", required: true },
  perfil: { type: String, enum: ["ADMIN", "SOLIDARIO", "MANCOMUNADO"], default: 'ADMIN', required: true }
}, {
  timestamps: true
});

const empresaSchema = mongoose.Schema({
  codigoCliente: { type: String, required: true, maxlength: 8 },
  codigoEmpresa: { type: String, required: true, maxlength: 8 },
  tipoDocumento: { type: String, required: true, maxlength: 1 },
  numeroDocumento: { type: String, required: true, maxlength: 11},
  razonSocial: { type: String, required: true },
  email: { type: String, required: true },
  telefono: { type: telefonoSchema, required: true },
  usuarios: { type: [usuarioSchema], required: true }
});

empresaSchema.methods.encryptPassword = async password => {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};

empresaSchema.methods.matchPassword = async function(password, indice) {
  return await bcrypt.compare(password, this.usuarios[indice].password);
};

export default mongoose.model('Empresa', empresaSchema);