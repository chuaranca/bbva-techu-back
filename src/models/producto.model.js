"use strict";

import mongoose from "mongoose";

const cuotasSchema = mongoose.Schema({
  _id: false,
  cuotaMinima: { type: Number, default: 2, required: true },
  cuotaMaxima: { type: Number, default: 12, required: true },
});

const parametriaSchema = mongoose.Schema({
  _id: false,
  tipoDisposicion: {
    type: [String],
    enum: ["N - VARIAS CUOTAS", "1 - UNA CUOTA"],
    required: true,
  },
  montoMinimo: { type: Number, required: true },
  cuotas: { type: cuotasSchema, required: true },
});

const productoSchema = mongoose.Schema({
  codigoCliente: { type: String, required: true, maxlength: 8 },
  numeroCuenta: { type: String, required: true },
  fechaAlta: { type: Date, default: Date.now, required: true },
  fechaVencimiento: { type: Date, default: Date.now, required: true },
  tipo: {
    type: String,
    enum: ["CUENTA FÁCIL", "CUENTA GANADORA", "CTA CREDITO"],
    default: "",
    required: true,
  },
  importeTotal: { type: Number, default: 0, required: true },
  importeDisponible: { type: Number, default: 0, required: true },
  divisa: { type: String, enum: ["PEN", "USD"], required: true },
  estado: { type: String, enum: ["A", "I", "P"], default: "A", required: true },
  parametrias: { type: parametriaSchema },
});

export default mongoose.model("Producto", productoSchema);
