'use strict';

import mongoose from "mongoose";

const movimientoSchema = mongoose.Schema({
  codigoCliente: { type: String, required: true },
  cuentaOrigen: { type: String, required: true },
  cuentaDestino: { type: String, required: true },
  fechaOperacion: { type: Date, default: Date.now },
  descripcion: { type: String, required: true },
  importeOperacion: { type: Number, required: true },
  divisa: { type: String, required: true }
});

export default mongoose.model('Movimiento', movimientoSchema);