'use strict';

import mongoose from "mongoose";
import telefonoSchema from "./telefono.schema";
import bcrypt from "bcryptjs";

const usuarioPersonaSchema = mongoose.Schema({
  codigoCliente: { type: String, required: true, unique: true, maxlength: 8 },
  tipoDocumento: { type: String, enum: ['R', 'L'], required: true },
  numeroDocumento: { type: String, required: true, unique: true , maxlength: 11},
  nombres: { type: String, required: true },
  apellidos: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  telefono: { type: telefonoSchema, required: true},
  perfil: { type: String, enum:["ADMIN", "CLIENTE"], required: true }
}, {
  timestamps: true
});

usuarioPersonaSchema.methods.encryptPassword = async password => {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};

usuarioPersonaSchema.methods.matchPassword = async function(password) {
  return await bcrypt.compare(password, this.password);
}

export default mongoose.model('UsuarioPersona', usuarioPersonaSchema);