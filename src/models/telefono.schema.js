'use strict';

import mongoose from "mongoose";

const telefonoSchema = mongoose.Schema({
  _id: false,
  numero: { type: String, required: true },
  operador: { type: String, required: true }
});

export default telefonoSchema;