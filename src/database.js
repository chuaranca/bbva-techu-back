"use strict";

import mongoose from "mongoose";

let URI_MONGODB;

if (process.env.ENV === "dev") {
  URI_MONGODB = process.env.MONGODB_URI_DEV;
} else {
  URI_MONGODB = process.env.MONGODB_URI_PROD;
}

console.log(`Entorno de Node JS: ${process.env.ENV}`);

mongoose
  .connect(URI_MONGODB, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(console.log("Se conectó a la base de datos..."))
  .catch((err) => console.log(err));
