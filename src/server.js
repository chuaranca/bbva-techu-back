'use strict';

import express from "express";
import cors from "cors";
import bodyParser from "body-parser";

import loginRoutes from "./routes/login.routes";
import usuarioRoutes from "./routes/usuario.routes";
import productoRoutes from "./routes/producto.routes";
import operacionRoutes from "./routes/operacion.routes";

import errorMiddleware from "./middlewares/error";
import { } from "./auth/auth"; 
import passport, { session } from "passport";

const app = express();
const API_VERSION = "/api/v1";

//Configuraciones
app.set('port', process.env.PORT || 3001);

//Configuration CORS in environment dev.
if (process.env.ENV === 'dev') {
  app.use(cors());
}

//Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Routes
app.use(API_VERSION, loginRoutes);
app.use(API_VERSION, passport.authenticate('jwt', { session: false }), usuarioRoutes);
app.use(API_VERSION, passport.authenticate('jwt', { session: false }), productoRoutes);
app.use(API_VERSION, passport.authenticate('jwt', { session: false }), operacionRoutes);

//Error Middleware
app.use(errorMiddleware);

export default app;
