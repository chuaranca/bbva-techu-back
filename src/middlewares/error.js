'use strict';

import { STATUS_ERROR, MSG_ERROR } from "../shared/constans";

const errorMiddleware = (err, req, res, next) => {
  let errorObject, statusCode;

  if (res.headerSent) return next(err);

  if (err.name === "ModelValidationError") {
    statusCode = err.status;
    errorObject = err.toJson();
  } else {
    //El error no controlado, debería de guardarse en un log
    console.error(err.message);
    statusCode = 500;
    errorObject = {
      status: STATUS_ERROR,
      message: MSG_ERROR,
    };
  }

  res.status(statusCode).send(errorObject);
};

export default errorMiddleware;