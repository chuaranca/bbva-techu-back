const validarPerfilMiddleware = (req, res, next) => {
  console.log('Inicio del middleware validarPerfilMiddleware()...');
  const { tipoDocumento, perfil } = req.body;
  console.log(`tipo de documento: ${tipoDocumento}, perfil: ${perfil}`);
  next();
};

export default validarPerfilMiddleware;