'use strict';

import passport from "passport";
import { Strategy as localStrategy } from "passport-local";
import { Strategy as JWTStrategy, ExtractJwt } from "passport-jwt";
import EmpresaModel from "../models/empresa.model";

/**
 * Middleware encargado de validar si la empresa y el usuario existen en nuestras bases, para
 * posterior a ello comparar las claves y retornar la empresa con el usuario logueado.
 */
passport.use('login-empresa', new localStrategy({
    usernameField: 'codigo',//codigoEmpresa|codigoUsuario
    passwordField: 'password'
}, async (username, password, done) => {
  console.log(username);
  const [codigoEmpresa, codigoUsuario] = username.split('|');//Destructuración

  try {
    let usuarioDb;
    let empresa = await EmpresaModel.findOne({ codigoEmpresa: codigoEmpresa });
    if (!empresa) return done(null, false, { message: 'Empresa no encontrada.' });

    empresa.usuarios.forEach(usuario => {
      if (usuario.codigoUsuario === codigoUsuario) {
        usuarioDb = usuario;
      }
    });

    if (!usuarioDb) return done(null, false, { message: 'Usuario no encontrado' });
    
    const validacion = await empresa.matchPassword(password, usuarioDb.__index);
    if (!validacion) return done(null, false, { message: 'Password Incorrecto.' });

    empresa.usuarios = usuarioDb;
    console.log(empresa);

    return done(null, empresa, { message: 'Inicio de sesión exitoso.' }); 
  } catch (error) {
    return done(error);    
  }
}));

/**
 * Middleware que verifica si el token enviado es válido
 */
passport.use(new JWTStrategy({
  secretOrKey: process.env.JWT_KEY,
  //Obtenemos el token desde el header.
  jwtFromRequest: ExtractJwt.fromHeader('access-token')
}, async (token, done) => {
  try {
    //Se retorna el usuario guardado en token al siguiente middleware.
    return done(null, token.user);
  } catch (error) {
    done(error);
  }
}));