export const STATUS_SUCCESS = 'SUCCESS';
export const STATUS_WARNING = 'WARNING';
export const STATUS_ERROR = 'ERROR';

export const MSG_SUCCESS = 'Operación realizada correctamente.';
export const MSG_ERROR = 'Servicio no disponible'