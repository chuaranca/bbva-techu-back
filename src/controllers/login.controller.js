'use strict';

import passport from "passport";
import jwt from "jsonwebtoken";

const loginController = {};

/**
 * Función que invoca al localStrategy de passport, esta función obtiene la empresa y
 * captura ciertos datos para encriptarlos en el JWT.
 */
loginController.loginEmpresa = async (req, res, next) => {
  passport.authenticate('login-empresa', (err, user, info) => {
    try {
      if (err || !user) return next(err);

      req.login(user, { session: false }, async (error) => {
        if (error) return next(error);

        const body = {
          codigoCliente: user.codigoCliente,
          codigoEmpresa: user.codigoEmpresa,
          codigoUsuario: user.usuarios[0].codigoUsuario,
          nombres: user.usuarios[0].nombres,
        }

        const token = jwt.sign({ user: body }, process.env.JWT_KEY, {
          expiresIn: 60 * 20,
        });
        return res.json({ token });
      });
    } catch (error) {
      next(error)
    }
  })(req, res , next);
};

export default loginController;