'use strict';

import EmpresaModel from "../models/empresa.model";
import ModelValidationError from "../errors/ModelValidationError";
import { STATUS_SUCCESS, MSG_SUCCESS, STATUS_WARNING } from "../shared/constans";

const usuarioEmpresaController = {};

/**
 * Función encargada de Crear una empresa con un usuario ADMIN001 por defecto, esta función, sólo es utilizada por el usuario admin de banco.
 */
usuarioEmpresaController.crearEmpresa = async (req, res, next) => {
  console.log('Inicio del método crearClienteEmpresa()');
  let empresaReq = new EmpresaModel();
  empresaReq = Object.assign(empresaReq, req.body);

  try {
    let empresa = await EmpresaModel.findOne({ codigoEmpresa: empresaReq.codigoEmpresa });
    if (empresa) throw new ModelValidationError(`La empresa con codigo ${empresaReq.codigoEmpresa} ya existe.`);

    empresa = await EmpresaModel.findOne({ numeroDocumento: empresaReq.numeroDocumento });
    if (empresa) throw new ModelValidationError(`La empresa con número de documento ${empresaReq.numeroDocumento} ya existe.`);

    empresa = await EmpresaModel.findOne({ email: empresaReq.email });
    if (empresa) throw new ModelValidationError(`La empresa con email ${empresaReq.email} ya existe.`);

    //empresaReq.usuarios[0].codigoUsuario = 'ADMIN001';//Esto se puede poner en el default del modelo
    empresaReq.usuarios[0].password = await empresaReq.encryptPassword('123456');

    await empresaReq.save();

    res.status(200).send({ status: STATUS_SUCCESS, message: MSG_SUCCESS });
  } catch (error) {
    next(error);
  }
};

/**
 * Función encargada de Listar todas las empresas, pero sin sus usuarios, esto debido a que mostrar con dicha información  sería muy
 * pesada a nivel de front.
 */
usuarioEmpresaController.listarEmpresas = async (req, res, next) => {
  console.log('Inicio del método listarEmpresas()');
  try {
    const empresas = await EmpresaModel.find({}, { usuarios: 0 });
    if (!empresas) throw new ModelValidationError(`No existen registros.`);

    res.status(200).send(empresas);
  } catch (error) {
    next(error);
  }
};

/**
 * Esta función se comportará igual que la función anterior con la única diferencia de que mostrará todos los datos de los 
 * usuarios.
 */
usuarioEmpresaController.obtenerEmpresaPorCodigoCliente = async (req, res, next) => {
  console.log('Inicio del método obtenerEmpresaPorCodigoCliente()');
  try {
    const codigoCliente = req.params.codigoCliente;
    if (codigoCliente) {
      const empresa = await EmpresaModel.findOne({ codigoCliente: codigoCliente });
      if (!empresa) throw new ModelValidationError(`La empresa con código ${codigoCliente}, no exsite.`);

      res.status(200).send(empresa);
    } else {
      res.status(200).send({ status: STATUS_WARNING, message: 'Ingresar un parámetro válido.' });
    }
  } catch (error) {
    next(error);
  }
};

/********************************* CRUD de usuarios de las empresas *********************************/

/**
 * Función encargada de registrar los usuarios de las empresas... Esta función sólo es usada por las mismas empresas
 */
usuarioEmpresaController.crearUsuarioEmpresa = async (req, res, next) => {
  console.log('Inicio del método crearUsuarioEmpresa()');
  try {
    //1. Validar si la empresa existe (consultar por codigoCliente)
    let codigoClienteReq = req.user.codigoCliente;
    let empresa = await EmpresaModel.findOne({ codigoCliente: codigoClienteReq });
    if (!empresa) throw new ModelValidationError(`La empresa con código ${codigoCliente}, no exsite.`);

    //2. Una vez obtenida la empresa, validamos datos únicos del usuario
    const { codigoUsuario, email } = req.body;
    empresa.usuarios.forEach(usuario => {
      if (usuario.codigoUsuario === codigoUsuario.toUpperCase()) throw new ModelValidationError(`El código de usuario ${codigoUsuario}, ya existe.`);
      if (usuario.email === email.toLowerCase()) throw new ModelValidationError(`El email ${email}, ya existe.`);
    });

    //3. Corregimos unos posibles datos para poder persistirlos
    req.body.codigoUsuario = codigoUsuario.toUpperCase();
    req.body.email = email.toLowerCase();
    req.body.password = await empresa.encryptPassword('123456');
    empresa.usuarios.push(req.body);

    await empresa.save();

    res.status(200).send({ status: STATUS_SUCCESS, message: MSG_SUCCESS });
  } catch (error) {
    next(error);
  }
};

/**
 * Función encargada de Listar todos los usuarios de la empresa logueada.
 */
usuarioEmpresaController.listarUsuariosEmpresa = async (req, res, next) => {
  console.log('Inicio del método listarUsuariosEmpresa()');
  try {
    let codigoClienteReq = req.user.codigoCliente;
    let empresa = await EmpresaModel.findOne({ codigoCliente: codigoClienteReq });
    if (!empresa) throw new ModelValidationError(`No existen registros.`);

    let usuariosActivos = new Array();
    empresa.usuarios.forEach(user => {
      if(user.estado =='A') usuariosActivos.push(user);
    });

    res.status(200).send(usuariosActivos);//Hay ciertos campos que no debemos de mandar.
  } catch (error) {
    next(error);
  }
};

usuarioEmpresaController.eliminarUsuarioEmpresa = async (req, res, next) => {
  console.log('Inicio del método eliminarUsuarioEmpresa()');

  try {
    //1- Consulto empresa
    const userEmpresa = await EmpresaModel.findOne({ codigoCliente: req.user.codigoCliente , codigoEmpresa: req.user.codigoEmpresa })
    if (!userEmpresa) throw new ModelValidationError(`No existen usuario.`);

    //2. Una vez obtenida la empresa, buscamos al usuario para ingresarle el valor de Inactivo
    userEmpresa.usuarios.forEach(user => {
      if (user.codigoUsuario == req.params.codigoUsuario ){
        user.estado = 'I';
      }
    });
    
    //3. Guardo la modficacion
    await userEmpresa.save();

    res.status(200).send({ status: STATUS_SUCCESS, message: MSG_SUCCESS });
  } catch (error) {
    next(error);
  }
};

usuarioEmpresaController.actualizarUsuarioEmpresa = async (req, res, next) => {
  console.log('Inicio del método actualizarUsuarioEmpresa()');

  try {
    //1- Consulto empresa
    const userEmpresa = await EmpresaModel.findOne({ codigoCliente: req.user.codigoCliente , codigoEmpresa: req.user.codigoEmpresa })
    if (!userEmpresa) throw new ModelValidationError(`No existen usuario.`);

    //2. Una vez obtenida la empresa, validamos datos únicos del usuario (email en estado activo)
    const { email } = req.body;
    userEmpresa.usuarios.forEach(usuario => {
      if (usuario.email === email.toLowerCase() && usuario.estado=='A' && usuario.codigoUsuario!=req.params.codigoUsuario) throw new ModelValidationError(`El email ${email}, ya existe.`);
    });

    //3. Corregimos unos posibles datos para poder persistirlos
    req.body.email = email.toLowerCase();

    //2. Una vez obtenida la empresa, buscamos al usuario para ingresarle el valor de Inactivo
    userEmpresa.usuarios.forEach(user => {
      if (user.codigoUsuario == req.params.codigoUsuario ){
        user.nombres = req.body.nombres;
        user.apellidos = req.body.apellidos;
        user.email = req.body.email;
        user.telefono.numero = req.body.telefono.numero;
        user.telefono.operador  = req.body.telefono.operador
      }
    });
    
    //3. Guardo la modficacion
    await userEmpresa.save();

    res.status(200).send({ status: STATUS_SUCCESS, message: MSG_SUCCESS });
  } catch (error) {
    next(error);
  }
};

export default usuarioEmpresaController;