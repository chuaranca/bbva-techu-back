"use strict";

import EmpresaModel from "../models/empresa.model";
import ModelValidationError from "../errors/ModelValidationError";
import ProductoModel from "../models/producto.model";
import { STATUS_SUCCESS, MSG_SUCCESS } from "../shared/constans";

const productoController = {};

/**
 * Función encargada de crear productos de las empresa, como por ejemplo las cuentas corrientes que apertura el cliente.
 * El codigo del cliente se obtiene desde JWT
 */
productoController.crearProductos = async (req, res, next) => {
  console.log("Inicio del método crearProductos()");
  try {
    //1. Validar si el codigo del cliente existe (osea la empresa)
    const empresaDb = await EmpresaModel.findOne(
      { codigoCliente: req.user.codigoCliente },
      { codigoCliente: true }
    );
    if (!empresaDb)
      throw new ModelValidationError(
        `La empresa con codigo ${req.user.codigoCliente}, no existe.`
      );

    //2. Verificar si todos los datos ingresados estan conforme
    let productoReq = new ProductoModel();
    Object.assign(productoReq, req.body);
    
    const randomValue = Math.floor(Math.random() * (99999999 - 11111111)) + 11111111;

    switch (req.body.tipo) {
      case "CF":
        productoReq.tipo = "CUENTA FÁCIL";
        productoReq.numeroCuenta = `0011000101${randomValue}`;
        break;
      case "CG":
        productoReq.tipo = "CUENTA GANADORA";
        productoReq.numeroCuenta = `0011000102${randomValue}`;
        break;
    }

    //3. Registrar la Operación
    productoReq.codigoCliente = req.user.codigoCliente;

    //productoReq.fechaVencimiento = Date.now;
    await productoReq.save();

    res.status(200).send({ status: STATUS_SUCCESS, message: MSG_SUCCESS });
  } catch (error) {
    next(error);
  }
};

/**
 * Funcion encargada de listar los productos activos de una empresa
 */
productoController.listarProductos = async (req, res, next) => {
  console.log("Inicio del método listarProductos()");
  try {
    const productos = await ProductoModel.find({
      codigoCliente: req.user.codigoCliente,
      estado: "A",
    });
    if (!productos) throw new ModelValidationError(`No existen registros.`);

    res.status(200).send(productos);
  } catch (error) {
    next(error);
  }
};

export default productoController;
