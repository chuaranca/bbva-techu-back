'use strict';

import requestJson from "request-json";

import MovimientoModel from "../models/movimiento.model";
import ModelValidationError from "../errors/ModelValidationError";
import ProductoModel from "../models/producto.model";
import { STATUS_SUCCESS, MSG_SUCCESS } from "../shared/constans";

const operacionController = {};

/**
 * Función encargada de consultar los movimientos de una cuenta
 */
operacionController.consultarMovimientos = async (req, res, next) => {
    console.log('Inicio del método consultarMovimientos()');
    
    try {
        const mov = await MovimientoModel.find({ codigoCliente: req.user.codigoCliente, cuentaOrigen: req.params.cuenta })
        if (!mov) throw new ModelValidationError(`No existen registros.`);
        
        res.status(200).send(mov);
    } catch (error) {
        next(error)
    }
};

operacionController.simtranspropia = async (req, res, next) => {
    console.log('Inicio del método simtranspropia()');
    try {
        const producto = await ProductoModel.findOne({ codigoCliente: req.user.codigoCliente, numeroCuenta: req.body.cuentaOrigen })
        if (!producto) throw new ModelValidationError(`No existen registros.`);
 
        if ((producto.importeDisponible > 0) && (producto.importeDisponible > req.body.importeOperacion)) {
            //let movReq = new MovimientoModel();
            //Object.assign(movReq, req.body);
            const movReq = {
              cuentaOrigen: req.body.cuentaOrigen,
              cuentaDestino: req.body.cuentaDestino,
              importeOperacion: req.body.importeOperacion,
              divisa: req.body.divisa
            }
            res.status(200).send(movReq);
        } else {
            throw new ModelValidationError(`Saldo Insuficiente.`);
        }
    } catch (error) {
        next(error)
    }
};

operacionController.realizartranspropia = async (req, res, next) => {
    console.log('Inicio del método realizartranspropia()');
    try {
        const producto = await ProductoModel.findOne({ codigoCliente: req.user.codigoCliente, numeroCuenta: req.body.cuentaOrigen })
        if (!producto) throw new ModelValidationError(`No existen registros.`);

        let impCuentaOrigen = producto.importeDisponible;
        let impOperacion = req.body.importeOperacion;

        if ((impCuentaOrigen > 0) && (impCuentaOrigen > impOperacion)) {

            impCuentaOrigen = impCuentaOrigen - impOperacion;
            await producto.updateOne({ importeDisponible: impCuentaOrigen });

            let prodDest = await ProductoModel.findOne({ codigoCliente: req.user.codigoCliente, numeroCuenta: req.body.cuentaDestino })
            if (producto.divisa == req.body.divisa) {
                prodDest.importeDisponible = prodDest.importeDisponible + impOperacion;
            } else {
                if (producto.divisa == 'PEN') {
                    prodDest.importeDisponible = prodDest.importeDisponible + (impOperacion / 3.4);
                }
                if (producto.divisa == 'USD') {
                    prodDest.importeDisponible = prodDest.importeDisponible + (impOperacion * 3.4);
                }
            }
            await prodDest.updateOne({ importeDisponible: prodDest.importeDisponible });

            //movimiento Cuenta Origen
            let movReq = new MovimientoModel();
            Object.assign(movReq, req.body);
            movReq.divisa = producto.divisa;
            movReq.descripcion = 'Transferencia'
            movReq.importeOperacion = (-1)*(req.body.importeOperacion);
            //3. Registrar la Operación
            movReq.codigoCliente = req.user.codigoCliente;
            await movReq.save();

            //movimiento Cuenta Destino
            let movDest = new MovimientoModel();
            Object.assign(movDest, req.body);
            movDest.descripcion = 'Ingreso x Transferencia'
            movDest.cuentaOrigen = req.body.cuentaDestino;
            movDest.cuentaDestino = req.body.cuentaOrigen;
            movDest.importeOperacion = req.body.importeOperacion;
            //3. Registrar la Operación
            movDest.codigoCliente = req.user.codigoCliente;
            await movDest.save();

            res.status(200).send({ status: STATUS_SUCCESS, message: MSG_SUCCESS });
        } else {
            throw new ModelValidationError(`Saldo no disponible.`);
        }
    } catch (error) {
        next(error)
    }
};

operacionController.simtcambio = async (req, res, next) => {
    console.log('Inicio del método simulacion de T-Cambio()');
    try {
        //Comprar PEN->USD , Vender USD->PEN
        let baseUrl = `${process.env.URL_BASE_TCAMBIO}/${req.body.divisaDestino}/${req.body.divisaOrigen}`;
        const params = `json?quantity=${req.body.importe}`;

        const uriTCambio = `${baseUrl}/${params}&Key=${process.env.APIKEY_TCAMBIO}`;
        const client = requestJson.createClient(uriTCambio);

        await client.get('', (err, resTcambio, body) => {
            try{
                if (req.body.divisaDestino == 'PEN') {body.result.value = (body.result.value)*10}
                res.status(200).send(body.result);
            }catch (error) {
                next(error)
            }
        });
    } catch (error) {
        next(error)
    }
};


operacionController.realizartCambio = async (req, res, next) => {
    console.log('Inicio del método realizartCambio()');
    try {        
        const producto = await ProductoModel.findOne({ codigoCliente: req.user.codigoCliente, numeroCuenta: req.body.cuentaOrigen })
        if (!producto) throw new ModelValidationError(`No existen registros.`);

        let impCuentaOrigen = producto.importeDisponible;
        let impOperacion = req.body.importeTCambio; 

        if ((impCuentaOrigen > 0) && (impCuentaOrigen > impOperacion)) {
            //Actualizao importe disponible cuenta origen
            impCuentaOrigen = impCuentaOrigen - impOperacion;
            await producto.updateOne({ importeDisponible: impCuentaOrigen });

            //Actualizao importe disponible cuenta destino
            let prodDest = await ProductoModel.findOne({ codigoCliente: req.user.codigoCliente, numeroCuenta: req.body.cuentaDestino })
            prodDest.importeDisponible = prodDest.importeDisponible + req.body.importeOperacion;
            await prodDest.updateOne({ importeDisponible: prodDest.importeDisponible });

            //registrar movimiento Cuenta Origen
            let movReq = new MovimientoModel();
            Object.assign(movReq, req.body);
            
            movReq.divisa = producto.divisa;
            movReq.descripcion = 'Compra TCambio'
            movReq.importeOperacion = (-1)*(req.body.importeTCambio);
            movReq.codigoCliente = req.user.codigoCliente;
            await movReq.save();

            //registrar movimiento Cuenta Destino
            let movDest = new MovimientoModel();
            Object.assign(movDest, req.body);
            
            movDest.descripcion = 'Transferencia de compra TCambio'
            movDest.divisa = prodDest.divisa;
            movDest.cuentaOrigen = req.body.cuentaDestino;
            movDest.cuentaDestino = req.body.cuentaOrigen;
            movDest.importeOperacion = req.body.importeOperacion;
            movDest.codigoCliente = req.user.codigoCliente;
            await movDest.save();

            res.status(200).send({ status: STATUS_SUCCESS, message: MSG_SUCCESS });
        } else {
            throw new ModelValidationError(`Saldo no disponible.`);
        }
    } catch (error) {
        next(error)
    }
};


export default operacionController;
