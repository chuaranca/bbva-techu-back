'use strict';

import PersonaModel from "../models/persona.model";
import ModelValidationError from "../errors/ModelValidationError";
import { STATUS_SUCCESS, MSG_SUCCESS, STATUS_WARNING } from "../shared/constans";

const usuarioPersonaController = {};

usuarioPersonaController.crearPersona = async (req, res, next) => {
  console.log('Inicio del método crearClientePersona()');
  let personaReq = new PersonaModel();
  personaReq = Object.assign(personaReq, req.body);

  try {
    let persona = await PersonaModel.findOne({ numeroDocumento: personaReq.numeroDocumento });
    if (persona) throw new ModelValidationError(`El cliente con número de documento ${personaReq.numeroDocumento} ya existe.`);

    persona = await PersonaModel.findOne({ email: personaReq.email });
    if (persona) throw new ModelValidationError(`El cliente con email ${personaReq.email} ya existe.`);

    await personaReq.save();

    res.status(200).send({status: STATUS_SUCCESS, message: MSG_SUCCESS});
  } catch (error) {
    next(error);
  }
};
