'use strict';

import express from "express";
import operacionController from "../controllers/operacion.controller";

const api = express.Router();

//Modulo Consultas Movimientos
api.get('/operacion/:cuenta', operacionController.consultarMovimientos);

// Modulo TransferenciaPropias
api.post('/operacion/simulartranspropia', operacionController.simtranspropia);
api.post('/operacion/realizartranspropia', operacionController.realizartranspropia);

//operacion Tcambio
api.post('/operacion/simulartcambio', operacionController.simtcambio);
api.post('/operacion/realizartCambio', operacionController.realizartCambio);

export default api;