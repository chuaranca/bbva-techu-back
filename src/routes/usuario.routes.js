'use strict';

import express from "express";
import usuarioEmpresaController from "../controllers/usuarioEmpresa.controller";
import loginController from "../controllers/login.controller";

let api = express.Router();

//Admin
api.get('/empresas', usuarioEmpresaController.listarEmpresas);
api.get('/empresas/:codigoCliente', usuarioEmpresaController.obtenerEmpresaPorCodigoCliente);
api.post('/empresas', usuarioEmpresaController.crearEmpresa);

//Empresa
api.get('/usuarios', usuarioEmpresaController.listarUsuariosEmpresa);
api.post('/usuarios', usuarioEmpresaController.crearUsuarioEmpresa);
api.delete('/usuarios/:codigoUsuario', usuarioEmpresaController.eliminarUsuarioEmpresa);
api.put('/usuarios/:codigoUsuario', usuarioEmpresaController.actualizarUsuarioEmpresa);

export default api;