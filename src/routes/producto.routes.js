'use strict';

import express from "express";
import productoController from "../controllers/producto.controller";

const api = express.Router();

api.post('/productos', productoController.crearProductos);
api.get('/productos', productoController.listarProductos);

export default api;