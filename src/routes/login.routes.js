'use stric';

import express from "express";
import loginController from "../controllers/login.controller";

const api = express.Router();

//Login
api.post('/empresas/login', loginController.loginEmpresa);

export default api;